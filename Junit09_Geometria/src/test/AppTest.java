package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import dto.Geometria;

/**
 * Unit test for simple App.
 */


public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
	
	Geometria cuadrado;
	Geometria circulo;
	Geometria triangulo;
	Geometria rectangulo;
	Geometria pentagono;
	Geometria rombo;
	Geometria romboide;
	Geometria trapecio;
	
	@Test
	public void contructor() {
		Geometria geometria = new Geometria();
	}
	
    @Test
    public void areaCuadrado()
    {
        int resultado = Geometria.areacuadrado(2);
        
        int esperado = 4;
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void areaCirculo()
    {
        double resultado = Geometria.areaCirculo(5);
        
        double esperado = 78.54;
        
        int delta = 1;
        
        assertEquals(esperado, resultado, delta);
    }
    
    @Test
    public void areaTriangulo()
    {
        int resultado = Geometria.areatriangulo(5,3);
        
        int esperado = 7;
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void areaRectangulo()
    {
        int resultado = Geometria.arearectangulo(2, 4);
        
        int esperado = 8;
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void areaPentagono()
    {
        int resultado = Geometria.areapentagono(5, 9);
        
        int esperado = 22;
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void areaRombo()
    {
        int resultado = Geometria.arearombo(5, 6);
        
        int esperado = 15;
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void areaRomboide()
    {
        int resultado = Geometria.arearomboide(6, 12);
        
        int esperado = 72;
        
        assertEquals(esperado, resultado);
    }

    @Test
    public void areaTrapecio()
    {
        int resultado = Geometria.areatrapecio(3, 5, 7);
        
        int esperado = 28;
        
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void figura() {
    	Geometria cuadrado = new Geometria(1);
        Geometria circulo = new Geometria(2);
        Geometria triangulo = new Geometria(3);
        Geometria rectangulo = new Geometria(4);
        Geometria pentagono = new Geometria(5);
        Geometria rombo = new Geometria(6);
        Geometria romboide = new Geometria(7);
        Geometria trapecio = new Geometria(8);
        Geometria defau = new Geometria(10);
    }
}
